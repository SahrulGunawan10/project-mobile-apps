import 'package:flutter/material.dart';
import 'package:flutter/src/foundation/key.dart';
import 'package:flutter/src/widgets/container.dart';
import 'package:flutter/src/widgets/framework.dart';
import 'package:medhealth/main_page.dart';
import 'package:medhealth/widget/button_primary.dart';
import 'package:medhealth/widget/general_logo_space.dart';
import 'package:medhealth/widget/widget_ilustration.dart';

class SuccesCheckout extends StatelessWidget {
  @override
  Widget build(BuildContext context) {
    return Scaffold(
      body: GeneralLogoSpace(
        child: ListView(
          padding: EdgeInsets.all(24),
          shrinkWrap: true,
          children: [
            SizedBox(
              height: 80,
            ),
            WidgetIlustration(
              image: "assets/order_success_ilustration.png",
              title: "Yeaayy, Checkout Telah Berhasil",
              subtitle1: "Temukan Product Kesehatan",
              subtitle2: "Kapanpun dan Dimanapun ",
            ),
            SizedBox(
              height: 30,
            ),
            ButtonPrimary(
              text: "BACK TO HOME",
              onTap: () {
                Navigator.pushAndRemoveUntil(
                    context,
                    MaterialPageRoute(
                      builder: (context) => MainPages(),
                    ),
                    (route) => false);
              },
            ),
          ],
        ),
      ),
    );
  }
}
