import 'package:flutter/material.dart';
import 'package:medhealth/theme.dart';

class WidgetIlustration extends StatelessWidget {
  final Widget child;
  final String image;
  final String title;
  final String subtitle1;
  final String subtitle2;

  WidgetIlustration(
      {this.child, this.image, this.title, this.subtitle1, this.subtitle2});
  @override
  Widget build(BuildContext context) {
    return Column(
      children: [
        Image.asset(
          image,
          width: 250,
        ),
        SizedBox(
          height: 40,
        ),
        Text(
          title,
          style: boldTextStyle.copyWith(fontSize: 20),
          textAlign: TextAlign.center,
        ),
        SizedBox(
          height: 30,
        ),
        Column(
          children: [
            Text(
              subtitle1,
              textAlign: TextAlign.center,
              style:
                  regulerTextStyle.copyWith(fontSize: 15, color: greyBoldColor),
            ),
            SizedBox(
              height: 8,
            ),
            Text(
              subtitle2,
              textAlign: TextAlign.center,
              style:
                  regulerTextStyle.copyWith(fontSize: 15, color: greyBoldColor),
            ),
            SizedBox(
              height: 80,
            ),
            child ?? SizedBox(),
          ],
        )
      ],
    );
  }
}
