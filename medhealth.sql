-- phpMyAdmin SQL Dump
-- version 5.2.0
-- https://www.phpmyadmin.net/
--
-- Host: 127.0.0.1
-- Waktu pembuatan: 20 Des 2022 pada 07.55
-- Versi server: 10.4.25-MariaDB
-- Versi PHP: 7.4.30

SET SQL_MODE = "NO_AUTO_VALUE_ON_ZERO";
START TRANSACTION;
SET time_zone = "+00:00";


/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8mb4 */;

--
-- Database: `medhealth`
--

-- --------------------------------------------------------

--
-- Struktur dari tabel `cart`
--

CREATE TABLE `cart` (
  `id_cart` int(11) NOT NULL,
  `id_user` int(11) NOT NULL,
  `id_product` int(11) NOT NULL,
  `quantity` int(11) NOT NULL,
  `price` int(11) NOT NULL,
  `created_at` datetime NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4;

--
-- Dumping data untuk tabel `cart`
--

INSERT INTO `cart` (`id_cart`, `id_user`, `id_product`, `quantity`, `price`, `created_at`) VALUES
(33, 2, 1, 1, 45000, '2022-12-20 13:54:20');

-- --------------------------------------------------------

--
-- Struktur dari tabel `category_product`
--

CREATE TABLE `category_product` (
  `id_category` int(11) NOT NULL,
  `category` varchar(150) NOT NULL,
  `image` text NOT NULL,
  `status` enum('on','off') NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4;

--
-- Dumping data untuk tabel `category_product`
--

INSERT INTO `category_product` (`id_category`, `category`, `image`, `status`) VALUES
(1, 'Antibiotics', 'assets/img_antibiotics.png', 'on'),
(2, 'Vitamins', 'assets/img_vitamins.png', 'on'),
(3, 'Digestive', 'assets/img_digestive.png', 'on'),
(4, 'Skin', 'assets/img_skin.png', 'on'),
(5, 'Cough and Flu', 'assets/img_cough_flu.png', 'on'),
(6, 'Fever', 'assets/img_fever.png', 'on'),
(7, 'Antiseptics', 'assets/img_antiseptics.png', 'on'),
(8, 'Covid-19 Info', 'assets/img_covid.png', 'on');

-- --------------------------------------------------------

--
-- Struktur dari tabel `orders`
--

CREATE TABLE `orders` (
  `id_orders` int(11) NOT NULL,
  `invoice` varchar(20) NOT NULL,
  `id_user` int(11) NOT NULL,
  `order_at` datetime NOT NULL,
  `status` int(11) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4;

-- --------------------------------------------------------

--
-- Struktur dari tabel `order_details`
--

CREATE TABLE `order_details` (
  `id_orders` int(11) NOT NULL,
  `invoice` varchar(20) NOT NULL,
  `id_product` int(11) NOT NULL,
  `quantity` int(11) NOT NULL,
  `price` int(11) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4;

-- --------------------------------------------------------

--
-- Struktur dari tabel `product`
--

CREATE TABLE `product` (
  `id_product` int(11) NOT NULL,
  `id_category` int(11) NOT NULL,
  `name` varchar(50) NOT NULL,
  `description` text NOT NULL,
  `image` text NOT NULL,
  `price` int(11) NOT NULL,
  `status` int(11) NOT NULL,
  `created_at` datetime NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4;

--
-- Dumping data untuk tabel `product`
--

INSERT INTO `product` (`id_product`, `id_category`, `name`, `description`, `image`, `price`, `status`, `created_at`) VALUES
(1, 1, 'CEFILA 200MG TAB 10S', 'Cefila merupakan antibiotik yang digunakan untuk mengatasi infeksi pada saluran pernafasan atas, infeksi saluran kemih dan kelamin, kulit dan jaringan lunak serta demam tifoid pada anak. Cefila mengandung Cefixime yaitu antibiotik spektrum luas golongan Sefalosporin generasi ketiga yang aktif terhadap bakteri Gram negatif maupun Gram positif. Cefixime bersifat bakteriosidal yang bekerja dengan cara mengikat satu atau lebih PBP (Penicillin-Binding Protein) pada sintesis dinding sel bakteri dengan memutus rantai polimer peptidoglikan sehingga tidak terbentuk. Hal tersebut dapat mengakibatkan kematian sel bakteri.', 'https://images.k24klik.com/product/apotek_online_k24klik_20211130110503359225_CEFILA-200MG-CAP-10S.jpg', 45000, 1, '2022-12-19 09:49:21'),
(2, 1, 'ZYCIN KMKES 500MG CAP 3S', 'ZYCIN KMKES 500MG CAP 3S merupakan antibiotik golongan makrolida yang mempunyai spektrum luas, aktif terhadap bakteri gram negatif maupun gram positif. Zycin bekerja dengan cara mengikat sub unit 50s dari ribosom bakteri sehingga menghambat translasi mRNA. Dengan demikian sistesis protein akan terganggu sehingga pertumbuhan bakteri akan terhambat.', 'https://images.k24klik.com/product/apotek_online_k24klik_20210729031223359225_ZYCIN-KMKES-500MG-CAP-3S.jpg', 60000, 1, '2022-12-19 09:50:38'),
(3, 1, 'ZITHROLIC KMKES 500MG FC CAPL 30S', 'ZITHROLIC KMKES 500MG FC CAPL 30S merupakan antibiotik golongan makrolida yang mempunyai spektrum luas, aktif terhadap bakteri gram negatif maupun gram positif. Zithrolic bekerja dengan cara mengikat sub unit 50s dari ribosom bakteri sehingga menghambat translasi mRNA. Dengan demikian sistesis protein akan terganggu sehingga pertumbuhan bakteri akan terhambat.', 'https://images.k24klik.com/product/apotek_online_k24klik_20211213092727359225_PXL-20211209-021430715-removebg-preview.png', 50000, 1, '2022-12-19 09:51:29'),
(4, 1, 'AMOXICILLIN 250 MG 10 KAPSUL', 'AMOXICILLIN merupakan antibiotik yang mengandung zat aktif Amoxicillin. Amoxicillin termasuk golongan antibiotik beta-lactam turunan ampicillin. \r\nObat ini digunakan pada infeksi saluran nafas atas, otitis media, bronkitis akut dan kronik, pneumonia, sistitis, uretritis, pielonefritis, bakteriuria asimptomatik pada masa hamil, gonore, infeksi kulit dan jaringan lunak. \r\nAmoxicillin memiliki spektrum mirip dengan ampisilin yaitu gram positif dan gram negatif. Bekerja dengan berikatan dengan penicillin-binding protein yang menghambat proses transpeptidation (proses cross-linking di sintesis dinding sel), mengaktivkan enzim autolisis pada dinding sel bakteri. Proses ini menyebabkan lisis dari dinding sel dan penghancuran sel bakteri. \r\nDalam penggunaan obat ini harus SESUAI DENGAN PETUNJUK DOKTER.', 'https://d2qjkwm11akmwu.cloudfront.net/products/2cdeb522-60e3-4ac7-b5f6-7a374ae52777_product_image_url.webp', 10000, 1, '2021-04-02 09:53:21'),
(5, 1, 'LEVOFLOXACIN NOVELL 500MG TAB 50S', 'LEVOFLOXACIN digunakan untuk mengobati infeksi bakteri seperti infeksi saluran kemih, pneumonia, sinusitis, infeksi kulit, jaringan lunak, dan infeksi prostat.Efek samping dari obat ini yaitu Diare, mual, vaginitis, kembung, pruritus, ruam kulit, nyeri abdomen, moniliasis genital, pusing, dispepsia, insomnia, gangguan pengecapan, muntah, anoreksia, ansietas, konstipasi, edema, lelah, sakit kepala, keringat berlebihan, leukore, tidak enak badan, gelisah, gangguan tidur, tremor, urtikaria.', 'https://images.k24klik.com/product/apotek_online_k24klik_201605170614461922_LEVOFLOXACIN-NOVELL-500MG.png', 18000, 1, '2022-12-19 09:54:38'),
(6, 2, 'PHARMATON FORMULA 5 KAPSUL', 'PHARMATON FORMULA merupakan kombinasi dari multivitamin, mineral dan ekstrak gingseng yang yang efektif mengurangi rasa letih, meningkatkan stamina pada saat aktivitas fisik, mendukung memori dan kemampuan belajar, serta dapat membantu menstabilkan emosi.', 'https://d2qjkwm11akmwu.cloudfront.net/products/81e69e67-a485-4a77-bab7-c9e6577b00cd_product_image_url.webp', 40000, 1, '2021-04-02 09:56:53'),
(7, 2, 'IMBOOST FORCE EXTRA STRENGTH 10 TABLET', 'IMBOOST FORCE EXTRA STRENGTH TABLET merupakan suplemen dengan kandungan Echinacea purpurea herb dry extract, Blackelderberry fruit dry extract, Zn Piccolinate dalam bentuk kaplet salut selaput. Suplemen ini digunakan untuk membantu meningkatkan serta memelihara daya tahan tubuh sehingga mencegah dari sakit dan mempercepat penyembuhan. \r\nImboost extra strength bekerja cepat mengaktifkan sistem daya tahan tubuh secara langsung di sistem pertahanan tubuh kita dengan memperbanyak antibodi sehingga daya tahan tubuh lebih kuat melawan serangan virus.', 'https://d2qjkwm11akmwu.cloudfront.net/products/bb39638d-3536-4cd7-934a-519ccc96dc4e_product_image_url.webp', 96500, 1, '2021-04-02 09:57:57'),
(8, 2, 'ENAPLEX C FC CAPL 6S STRIP 10S', 'ENAPLEX C FC CAPL 6S STRIP 10S merupakan suplemen vitamin untuk membantu menjaga daya tahan tubuh dengan kombinasi vitamin C dan vitamin B kompleks yang diperlukan untuk tubuh yang kekurangan vitamin, supaya daya tahan tubuh tetap terjaga serta membantu memulihkan kondisi setelah sakit. Enaplex C FC Capl memiliki kandungan: 1.Vitamin C (Ascorbic Acid) 2.Vitamin B1 (Thiamin Mononitrat) 3.Vitamin B2 (Riboflavin) 4.Vitamin B3 (Niacinamide) 5.Vitamin B5 (Kalsium Pantotenat) 6.Vitamin B6 (Pyridoxine HCl) 7.Vitamin B12 (Cyanocobalamin) Enaplex C FC Capl dapat dikonsumsi setelah makan untuk menghindari rasa tidak nyaman pada Gastrointestinal. Beli ENAPLEX C FC CAPL 6S STRIP 10S di K24Klik dan dapatkan manfaatnya.', 'https://images.k24klik.com/product/apotek_online_k24klik_2020101301540723085_ENAPLEX.jpg', 20000, 1, '2022-12-18 09:57:57'),
(9, 2, 'ENERVON-C 30 TABLET', 'ENERVON C merupakan suplemen makanan dengan kandungan multivitamin seperti Vitamin C, Vitamin B1, Vitamin B2, Vitamin B6, Vitamin B12, Vitamin D, Niasinamide, Kalsium pantotenat dalam bentuk tablet salut. \r\nSuplemen vitamin ini berguna untuk membantu menjaga daya tahan tubuh. Kombinasi vitamin C dan B komplek yang diperlukan untuk tubuh yang kekurangan vitamin, supaya daya tahan tubuh tetap terjaga serta membantu memulihkan kondisi setelah sakit.', 'https://d2qjkwm11akmwu.cloudfront.net/products/ceaa46b9-b668-4d9b-988f-909c0d42a141_product_image_url.webp', 55000, 1, '2021-04-02 10:00:05'),
(10, 2, 'BIO FORMULA CAPL 4S STRIP 5S', 'BIO FORMULA CAPL 4S STRIP 5S merupakan multivitamin yang mengandung Ekstrak gingseng, Beta karoten, Vitamin B1, Vitamin B2, Vitamin B3, Vitamin B5, Vitamin B6, Vitamin B12, Vitamin C, Vitamin D, Vitamin E, Asam Folat, Kalsium, Fe dan Zinc yang berfungsi untuk menjaga daya tahan tubuh serta memelihara kesehatan tubuh.', 'https://images.k24klik.com/product/apotek_online_k24klik_20211201093349359225_bio-formula-capl-4s-strip-removebg-preview.png', 15000, 1, '2022-12-19 10:00:05'),
(11, 3, 'POLYSILANE SIRUP 100 ML', 'POLYSILANE SIRUP merupakan obat maag dan anti kembung dengan kandungan Dimetilpolisiloksan, Aluminium hidroksida, dan Magnesium hidroksida dalam bentuk sirup. \r\nObat ini digunakan untuk mengurangi gejala yang berhubungan dengan kelebihan asam lambung, gastritis, tukak lambung, tukak usus 12 jari, dengan gejala-gejala seperti mual, nyeri lambung, nyeri ulu hati, kembung dan perasaan penuh pada lambung.', 'https://d2qjkwm11akmwu.cloudfront.net/products/b9b8f712-95a3-43eb-b511-e6ee9159c18c_product_image_url.webp', 25000, 1, '2021-04-02 10:04:20'),
(12, 3, 'NEW ENZYPLEX TAB 4S STRIP 25S', 'Enzyplex merupakan obat yang digunakan untuk membantu melancarkan proses metabolisme dan pencernaan di dalam tubuh dengan mencukupi kebutuhan enzim agar tetap optimal. Komposisi nya yaitu amilase 10000 sat, protease 9000 sat, lipase 240 sat, asam desoksikholat 30 mg, dimetilpolisiloksan, vitamin b1 10 mg, vitamin b2 5 mg, vitamin b6 5 mg, vitamin b12 5 mcg, niasinamid 10 mg, kalsium pantotenat 5 mg', 'https://images.k24klik.com/product/apotek_online_k24klik_20210323011733359225_NEW-ENZYPLEX-4.jpg', 9000, 1, '2022-12-19 10:04:20'),
(13, 3, 'PROMAG 12 TABLET', 'PROMAG merupakan obat dengan kandungan Hydrotalcite, Mg(OH)2, Simethicone dalam bentuk tablet. Obat ini digunakan untuk mengurangi gejala-gejala yang berhubungan dengan: kelebihan asam lambung, gastritis, tukak lambung, tukak usus 12 jari. Gejala seperti mual, nyeri lambung, nyeri ulu hati, kembung dan perasaan penuh pada lambung.', 'https://d2qjkwm11akmwu.cloudfront.net/products/881948_2-12-2021_16-26-6-1665834167.webp', 8000, 1, '2021-04-02 10:04:20'),
(14, 4, 'DERMATIX ULTRA GEL 15 G', 'DERMATIX ULTRA GEL 15 G merupakan obat yang digunakan untuk membantu menyamarkan bahkan menghilangkan bekas luka.', 'https://d2qjkwm11akmwu.cloudfront.net/products/4e225a58-5096-40db-bc4b-45caead0ef98_product_image_url.webp', 23500, 1, '2021-04-02 10:09:16'),
(15, 4, 'KALPANAX K CREAM 5 G', 'KALPANAX K CREAM mengandung Miconazole yang merupaka obat anti jamur golongan imidazole, digunakan untuk mengobati penyakit kulit akibat infeksi jamur.', 'https://images.k24klik.com/product/0116b0038.jpg', 27000, 1, '2021-04-02 10:09:16'),
(16, 5, 'BISOLVON EXTRA SIRUP 60 ML', 'BISOLVON EXTRA SIRUP mengandung Bromhexine HCl dan Guaifenesin. Obat ini digunakan untuk mengatasi batuk berdahak yang bekerja sebagai sekretolitik (mukolitik) dan ekspektoran untuk meredakan batuk berdahak dan mempermudah pengeluaran dahak pada saat batuk. \r\nObat ini akan membantu dengan 3 langkah kerja, yaitu: dengan Melepaskan, Mengencerkan, serta Mengeluarkan dahak, sehingga dahak dapat mudah dikeluarkan.', 'https://d2qjkwm11akmwu.cloudfront.net/products/208a7044-dbea-4614-af2d-ba4ede65ba53_product_image_url.webp', 42000, 1, '2021-04-02 10:11:34'),
(17, 5, 'OBH COMBI PLUS BATUK FLU 100 ML', 'OBH COMBI PLUS BATUK FLU adalah obat yang digunakan untuk meredakan batuk yang disertai gejala-gejala flu seperti demam, sakit kepala, hidung tersumbat, dan bersin-bersin. OBH Combi Plus Batuk Flu Menthol bekerja sebagai ekspektoran (membantu mengeluarkan dahak), antihistamin (mengurangi gejala alergi, analgesik-antipiretik (menurunkan panas, dmeam, dan meredakan sakit kepala), dan dekongestan hidung (melonggarkan saluran pernafasan).', 'https://d2qjkwm11akmwu.cloudfront.net/products/5c4dad7f-9fc9-4c50-bfca-35c78033c8a6_product_image_url.webp', 33500, 1, '2021-04-02 10:12:21'),
(18, 5, 'PARAMEX FLU BATUK 4S', 'Paramex Flu dan Batuk merupakan obat untuk meringankan gejala flu seperti demam, sakit kepala, hidung tersumbat dan bersin-bersin yang disertai batuk kering atau tidak berdahak. komposisi nya yaitu Paracetamol 500mg, Pseudoefedrin HCl 30mg, dextromethorphan HBr 15mg.', 'https://images.k24klik.com/product/apotek_online_k24klik_20200713083143359225_PARAMEX-FLU-500MG-1.jpg', 5000, 1, '2022-04-13 10:12:21'),
(19, 6, 'PARACETAMOL 500 MG 10 KAPLET', 'PARACETAMOL TABLET merupakan obat yang dapat digunakan untuk meringankan rasa sakit pada sakit kepala, sakit gigi, dan menurunkan demam. Paracetamol sebagai analgetik, bekerja dengan meningkatkan ambang rasa sakit dan sebagai antipiretik yang diduga bekerja langsung pada pusat pengatur panas di hipotalamus.', 'https://d2qjkwm11akmwu.cloudfront.net/products/de2e8223-2622-4861-a47f-64b14e8ed8d5_product_image_url.webp', 10000, 1, '2021-04-02 10:14:23'),
(20, 6, 'KOOLFEVER FOR ADULT 1 SACHET', 'KOOLFEVER ADULT merupakan plester kompres penurun panas dan pereda nyeri, serta dapat memberikan efek dingin di saat cuaca panas pada orang dewasa. Plester ini dapat memberikan sensasi dingin hingga 8 jam.', 'https://d2qjkwm11akmwu.cloudfront.net/products/8f3609e7-20ca-468f-a7ac-8d3360ece9d0_product_image_url.webp', 18000, 1, '2021-04-02 10:14:23'),
(21, 6, 'IBUPROFEN 400 MG 10 TABLET', 'IBUPROFEN merupakan obat generik dimana pada kadar 400 mg atau lebih digunakan untuk rasa nyeri dan inflamasi sebagai gejala utama. Obat ini digunakan sebagai analgesik yaitu untuk meringankan nyeri ringan sampai sedang antara lain nyeri pada sakit kepala, nyeri haid, nyeri pada penyakit gigi atau pencabutan gigi, dan nyeri setelah operasi. \r\nSelain itu, obat ini digunakan sebagai analgesik dan anti inflamasi dengan meringankan gejala-gejala penyakit rematik tulang, sendi dan non sendi, meringankan gejala-gejala akibat trauma otot dan tulang/sendi. Ibuprofen adalah golongan obat anti inflamasi non steroid yang mempunyai efek anti inflamasi, analgesik dan antipiretik. Obat ini bekerja dengan cara menghambat prostaglandin. \r\nDalam penggunaan obat ini harus SESUAI DENGAN PETUNJUK DOKTER.', 'https://d2qjkwm11akmwu.cloudfront.net/products/7689ed7d-1187-46a8-b919-978989e010ea_product_image_url.webp', 12500, 1, '2021-04-02 10:15:54'),
(22, 7, 'BETADINE SKIN ANTISEPTIC 100 ML', 'BETADINE SKIN CLEANSER ANTISEPTIC 100 ML sabun cair antiseptik yang mengandung Povidon iodine 7,5% untuk mengatasi masalah pada kulit seperti jerawat dan gatal-gatal, disinfeksi tangan sebelum operasi, dan menjaga kesehatan kulit.', 'https://d2qjkwm11akmwu.cloudfront.net/products/bc8da276-31b7-4148-9254-5229e3d25dea_product_image_url.webp', 45000, 1, '2021-04-02 10:17:36'),
(23, 7, 'DETTOL ANTISEPTIK CAIR 245 ML', 'DETTOL ANTISEPTIK CAIR merupakan antiseptik cair yang digunakan sebagai perlindungan dari penyakit yang disebabkan kuman. Cairan ini juga dapat digunakan untuk mempercepat penyembuhan luka, lecet, gigitan, sengatan serangga, membunuh kuman pada pakaian kotor dan sebagai disinfektan pada peralatan rumah tangga.', 'https://d2qjkwm11akmwu.cloudfront.net/products/02ac32c9-de61-4d68-91cf-92b4ed77525d_product_image_url.webp', 55000, 1, '2021-04-02 10:17:36'),
(24, 7, 'ANTIS JERUK NIPIS SPRAY 55 ML', 'ANTIS JERUK NIPIS SPRAY 55 ML merupakan pembersih tangan dalam bentuk spray dengan aroma jeruk nipis yang mengandung moisturaizer yang dapat melembabkan kulit dan dapat membunuh kuman.', 'https://d2qjkwm11akmwu.cloudfront.net/products/d138f207-6671-4d53-b372-497cc51a265d_product_image_url.webp', 15000, 1, '2021-04-02 10:19:29'),
(25, 4, 'HOT IN CREAM TUBE 60G', 'Hot in Cream merupakan cream anti nyeri yang sangat tepat untuk mengatasi capek, pegal-pegal dan nyeri otot yang mengganggu aktivitas harian. Diformulasikan dalam basis cream, nyaman dipakai, panasnya pas, mudah meresap, tidak lengket dikulit, dan tidak mengotori baju. Beli Hot in Cream tube 60G di Toko Online MEDHEALTH dan dapatkan manfaatnya.', 'https://images.k24klik.com/product/apotek_online_k24klik_20200807025016359225_HOT-IN-CREAM-60-G.jpg', 23000, 1, '2022-12-17 18:57:05'),
(26, 1, 'CLANEKSI 500MG TAB', 'Claneksi merupakan antibiotik kombinasi yang digunakan untuk mengatasi infeksi pada saluran pernafasan atas, infeksi saluran kemih, saluran cerna, kulit dan jaringan lunak. Claneksi mengandung kombinasi Amoksisilin (antibiotik golongan beta-laktam) dengan Asam Klavulanat (penghambat enzim beta-laktamase) yang disebut coamoxiclav. Coamoxiclav bersifat bakteriolitik yang bekerja dengan cara menghambat sintesis dinding sel bakteri dengan memutus rantai polimer peptidoglikan sehingga tidak terbentuk. Kombinasi ini bertujuan untuk meningkatkan efektivitas antibiotik terhadap bakteri-baketri yang memproduksi beta-laktamase yang resisten terhadap Amoksisilin.', 'https://images.k24klik.com/product/apotek_online_k24klik_20200928020214359225_claneksi.jpg', 20000, 1, '2022-12-19 04:31:25'),
(27, 2, 'VIDORAN GUMMY VITAMIN C 5S', 'Kembang gula lunak Jeli rasa jeruk yang tinggi vitamin C. Beli Vidoran Gummy Tinggi Vitamin C di k24klik dan dapatkan manfaatnya. Komposisinya terdiri dari Sirup Glukosa, Gula Pasir , Air, Gelatin Sapi, Pengatur Keasaman Asam Sitrat dan Asam Laktat, Pemanis Alami Sorbitol, Vitamin C, Perisa Identik Alami Jeruk, Pemanis Alami Manitol, Laktosa, Pewarna Kuning FCF Cl 15985', 'https://images.k24klik.com/product/apotek_online_k24klik_2020043002221023085_nHBfsgAAvAAAAAcAIiv5QgAGJlU.png', 16000, 1, '2022-12-19 04:34:43'),
(28, 3, 'L-BIO 30 SACH', 'L-Bio 30 Sachet adalah obat yang digunakan untuk melindungi sistem pencernaan dan memperbaiki fungsi normal saluran pencernaan ketika mengalami kondisi diare, konstipasi dan penggunaan antibiotika jangka panjang pada bayi, anak-anak ataupun dewasa. L-Bio mengandung probiotik atau bakteri baik seperti salah satunya Lactobacillus Acidophilus, Lactobacillus Casei dan Lactococcus Lactis yang bekerja dengan melawan pertumbuhan bakteri jahat supaya sistem pencernaan selalu sehat. ', 'https://images.k24klik.com/product/apotek_online_k24klik_20191216084736303669_L-BIO-SACH-30S.jpg', 10000, 1, '2022-12-19 05:00:28'),
(29, 3, 'LACIDOFIL SACH 30S', 'Lacidofil merupakan obat yang digunakan untuk melindungi sistem pencernaan dan memperbaiki fungsi normal saluran pencernaan ketika mengalami kondisi diare, konstipasi dan penggunaan antibiotika jangka panjang pada bayi, anak-anak ataupun dewasa.Lacidofil mengandung probiotik atau bakteri baik seperti salah satunya Lactobacillus acidophilus Rossel-52 dan Lactococcus rhamnosus Rossel-11 yang bekerja dengan melawan pertumbuhan bakteri jahat supaya sistem pencernaan selalu sehat', 'https://images.k24klik.com/product/apotek_online_k24klik_201904181124384677_LECIDOFIL.jpg', 9000, 1, '2022-12-19 05:02:55'),
(30, 3, 'LIPROLAC KIDS VANILA 2.5G SACH 30S', 'Liprolac merupakan produk yang mengandung kombinasi 5 probiotik dan FOS sebagai prebiotik, yang membantu memelihara kesehatan pencernaan anak. dikemas dalam bentuk bubuk untuk cairan oral dengan rasa enak yang disukai anak-anak. Liprolac juga mengandung vitamin dan mineral sebagai suplemen makanan. Beli Liprolac Sachet 2,5gr di apotek online K24klik dan dapatkan berbagai manfaatnya. (Kemasan: 1 dos isi 30 pcs).', 'https://images.k24klik.com/product/apotek_online_k24klik_2022092903282823085_liprolac.jpg', 10500, 1, '2022-12-19 05:05:34'),
(31, 4, 'NATUR E NOURISHING CALMING LOT 100ML', 'NATUR E NOURISHING CALMING LOT 100ML merupakan produk Natur E yang diindikasikan untuk kulit sensitif karena mengandung lebih banyak pelembab. Mampu mengatasi kulit kering yang mudah teriritasi dan merah. Natur E Daily Nourishing – Calming mengandung Blue Daisy yang bisa membantu mengurangi efek inflamasi dikulit sehingga kulit tidak teriritasi dan gatal. Adapun shea butter dan vegetable squalene merupakan pelembab yang optimal dalam membantu mengurangi kulit iritasi dan kering.', 'https://images.k24klik.com/product/apotek_online_k24klik_201901211128264677_natur-e-calming.jpeg', 57000, 1, '2022-12-19 05:32:02');

-- --------------------------------------------------------

--
-- Struktur dari tabel `user`
--

CREATE TABLE `user` (
  `id_user` int(11) NOT NULL,
  `name` varchar(50) NOT NULL,
  `email` varchar(50) NOT NULL,
  `phone` varchar(50) NOT NULL,
  `address` text NOT NULL,
  `password` text NOT NULL,
  `created_at` datetime NOT NULL,
  `status` int(11) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4;

--
-- Dumping data untuk tabel `user`
--

INSERT INTO `user` (`id_user`, `name`, `email`, `phone`, `address`, `password`, `created_at`, `status`) VALUES
(1, 'Saturnus', 'saturnus@gmail.com', '2147483647', 'Marss', '06badc29287e80322d51e26ba2e8af37', '2022-12-20 13:48:03', 1),
(2, 'Sahrul Gunawan ', 'sahrul@gmail.com', '085748540644', 'Sumberbaru ', '6fcf24311cfb6cca866572474e20029b', '2022-12-20 13:53:49', 1);

--
-- Indexes for dumped tables
--

--
-- Indeks untuk tabel `cart`
--
ALTER TABLE `cart`
  ADD PRIMARY KEY (`id_cart`);

--
-- Indeks untuk tabel `category_product`
--
ALTER TABLE `category_product`
  ADD PRIMARY KEY (`id_category`);

--
-- Indeks untuk tabel `orders`
--
ALTER TABLE `orders`
  ADD PRIMARY KEY (`id_orders`);

--
-- Indeks untuk tabel `order_details`
--
ALTER TABLE `order_details`
  ADD PRIMARY KEY (`id_orders`);

--
-- Indeks untuk tabel `product`
--
ALTER TABLE `product`
  ADD PRIMARY KEY (`id_product`),
  ADD KEY `id_category` (`id_category`);

--
-- Indeks untuk tabel `user`
--
ALTER TABLE `user`
  ADD PRIMARY KEY (`id_user`);

--
-- AUTO_INCREMENT untuk tabel yang dibuang
--

--
-- AUTO_INCREMENT untuk tabel `cart`
--
ALTER TABLE `cart`
  MODIFY `id_cart` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=34;

--
-- AUTO_INCREMENT untuk tabel `category_product`
--
ALTER TABLE `category_product`
  MODIFY `id_category` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=9;

--
-- AUTO_INCREMENT untuk tabel `orders`
--
ALTER TABLE `orders`
  MODIFY `id_orders` int(11) NOT NULL AUTO_INCREMENT;

--
-- AUTO_INCREMENT untuk tabel `order_details`
--
ALTER TABLE `order_details`
  MODIFY `id_orders` int(11) NOT NULL AUTO_INCREMENT;

--
-- AUTO_INCREMENT untuk tabel `product`
--
ALTER TABLE `product`
  MODIFY `id_product` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=32;

--
-- AUTO_INCREMENT untuk tabel `user`
--
ALTER TABLE `user`
  MODIFY `id_user` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=3;

--
-- Ketidakleluasaan untuk tabel pelimpahan (Dumped Tables)
--

--
-- Ketidakleluasaan untuk tabel `product`
--
ALTER TABLE `product`
  ADD CONSTRAINT `product_ibfk_1` FOREIGN KEY (`id_category`) REFERENCES `category_product` (`id_category`);
COMMIT;

/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
